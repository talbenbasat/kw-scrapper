import re

import requests
from bs4 import BeautifulSoup


# todo - handle split into parts and sub functions
def parse_page(page='http://ogp.me/'):
    r = requests.get(page)
    soup = BeautifulSoup(r.text, "html.parser")
    open_graph_elements = soup.find_all('meta', property=re.compile('^og:'))

    # todo - add timestap and id
    open_graph_dict = dict()

    for element in open_graph_elements:
        open_graph_sub_dict = open_graph_dict

        property_attr = element.attrs.get('property', None)
        content_attr = element.attrs.get('content', None)
        if not property_attr:
            continue

        property_arr = property_attr.replace('og:', '').split(':')
        property_path = property_arr[:-1]
        property_key = property_arr[-1]

        for item in property_path:
            open_graph_sub_dict_parent = open_graph_sub_dict
            open_graph_sub_dict = open_graph_sub_dict.get(item, None)
            # skip if the spec arrive before the basic
            if not open_graph_sub_dict:
                # log error
                continue

        # object convert if not json and property has additional spec
        if not isinstance(open_graph_sub_dict, dict):
            open_graph_sub_dict_parent[property_path[-1]] = dict()
            open_graph_sub_dict, value = open_graph_sub_dict_parent[property_path[-1]], open_graph_sub_dict
            # image and audio has url, locale is a bit different
            key = 'value' if value == 'locale' else 'url'
            open_graph_sub_dict[key] = value

        if open_graph_sub_dict.get(property_key, None):
            # array convert if not list and has multiple values
            if not isinstance(open_graph_sub_dict[property_key], list):
                value = open_graph_sub_dict[property_key]
                open_graph_sub_dict[property_key] = []
                open_graph_sub_dict[property_key].append(value)
                open_graph_sub_dict[property_key].append(content_attr)
            else:
                open_graph_sub_dict[property_key].append(content_attr)
        else:
            open_graph_sub_dict[property_key] = content_attr

    return open_graph_dict
