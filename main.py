import os
import datetime
import json
import logging
import logging.config
from urllib.parse import urlparse

from flask_sqlalchemy import SQLAlchemy
from flask import Flask, render_template, request, jsonify

from ogp import parse_page
from logging_conf import DEFAULT_LOGGING

logging.config.dictConfig(DEFAULT_LOGGING)
logger = logging.getLogger(__name__)

template_dir = os.path.abspath('templates')
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
db = SQLAlchemy(app)


class Page(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String(120), unique=True, nullable=False)
    scrape_status = db.Column(db.String(120), nullable=False)
    ogp = db.Column(db.String(2048))

    def __repr__(self):
        return '<Page %r>' % self.url


@app.route('/stories/<page_id>', methods=['GET'])
def get_ogp(page_id):
    try:
        logger.info("Received call for id {}".format(page_id))
        page = Page.query.filter_by(id=page_id).first()

        response = dict()
        response["id"] = page_id
        response["updated_time"] = str(datetime.datetime.now())

        if not page:
            logger.info("id {} not found returning error".format(page_id))
            response["scrape_status"] = "error"
            return jsonify(response)

        response["scrape_status"] = page.scrape_status
        if page.scrape_status != "done":
            logger.info("id {} not done returning {}".format(page_id, page.scrape_status))
            return jsonify(response)

        response.update(json.loads(page.ogp))
        return jsonify(response)

    except Exception as e:
        logger.exception(e)
        response = dict()
        response["id"] = page_id
        response["updated_time"] = str(datetime.datetime.now())
        response["scrape_status"] = "error"
        return jsonify(response)


@app.route('/stories', methods=['POST'])
def scrap_ogp():
    page = None
    url = request.args.get('url')

    try:
        logger.info("Received a post request for this url - {}".format(url))

        if not urlparse(url).netloc:
            logger.info("Fixing page schema")
            url = "http://" + url

        page = Page.query.filter_by(url=url).first()

        if not page:
            page = Page(url=url, scrape_status='pending')

            db.session.add(page)
            db.session.commit()

        response = parse_page(url)
        page.ogp = json.dumps(response)
        page.scrape_status = "done"
        db.session.commit()

        response["id"] = page.id
        response["updated_time"] = str(datetime.datetime.now())
        response["scrape_status"] = "done"

    except Exception as e:
        logger.exception(e)

        response = dict()
        response["scrape_status"] = "error"

        if page:
            page.scrape_status = "error"
            db.session.commit()
            response["id"] = page.id

        return jsonify(response)

    return jsonify(response)


@app.route('/', methods=['GET'])
def add_ogp():
    return render_template('new.html')


if __name__ == '__main__':
    db.create_all()
    app.run(host='0.0.0.0')
