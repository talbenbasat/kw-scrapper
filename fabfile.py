import os
import time

from fabric.api import *

############################################################
# Environment Variables and Defaults
############################################################
env.hosts = []
# specify path to files being deployed
env.archive_source = '.'
# archive name, arbitrary, and only for transport
env.archive_name = 'release'
# specify name of dir that will hold all deployed code
env.deploy_release_dir = 'releases'
# symlink name. Full path to deployed code is env.deploy_project_root + this
env.deploy_current_dir = 'kw'


def server1():
    env.hosts = ['talbenbasat.hiring.keywee.io']
    env.user = 'ubuntu'
    env.key_filename = './scraper-machine.pem'


############################################################
# Helper Functions
############################################################
# Todo change to env variables
def update_local_copy():
    # get latest / desired tag from your version control system
    if env.static:
        return

    print('updating local copy...')
    local('git pull https://{}:{}@bitbucket.org/rasrobotics/ras-robot.git'.format(
        os.getenv('JENKINS_USER'), os.getenv('JENKINS_PASSWORD')))


def upload_archive():
    # create archive from env.archive_source
    print('creating archive...')
    local('cd {} && zip -qr {}.zip -x=fabfile.py -x=fabfile.pyc *'.format(env.archive_source, env.archive_name))

    # create time named dir in deploy dir
    print('uploading archive...')
    deploy_timestring = time.strftime("%Y%m%d%H%M%S")
    env.deploy_full_path = env.deploy_project_root + env.deploy_release_dir + '/' + deploy_timestring
    sudo('chmod 777 -R {}'.format(env.deploy_project_root))
    run('mkdir -p {}'.format(env.deploy_full_path))

    # extract code into dir
    print('extracting code...')
    put(env.archive_name + '.zip', env.deploy_full_path)
    run('cd {} && unzip -q {}.zip -d . && rm {}.zip'.format(env.deploy_full_path, env.archive_name, env.archive_name))


def create_log_dir():
    # creating a dir in /var/log/ for logging output
    print('creating dir /var/log/kw...')
    sudo('mkdir -p /var/log/kw/kw-scraper/')
    print('changing dir mod...')
    sudo('chmod -R 755 /var/log/kw/kw-scraper/')


def install_essentials():
    sudo('apt-get install nginx -y')
    sudo('apt-get install unzip -y')
    sudo('apt-get install python3-dev -y')
    sudo('apt-get install libhiredis-dev -y')


def copy_code():
    # delete existing symlink & replace with symlink to deploy_timestring dir
    print('creating symlink to uploaded code...')
    sudo('rm -rf {}'.format(env.deploy_project_root + env.deploy_current_dir))
    run('cp -r {} {}'.format(env.deploy_full_path, env.deploy_project_root + env.deploy_current_dir))


def configure_services():
    # code is live, perform any post-deploy tasks here
    print('after symlink tasks...')
    path = '/etc/systemd/system/scraper.service'
    sudo('rm -rf {}'.format(path))
    sudo('cp {} {}'.format(
        env.deploy_project_root + env.deploy_current_dir + '/scraper.service',
        path
    ))

    sudo("systemctl daemon-reload")

    sudo("systemctl enable scraper.service")
    sudo("systemctl start scraper.service")


def create_version():
    version = local('git log  -n 1', capture=True)
    sudo('echo "{}" >> {}kw/kw-scraper/version.txt'.format(version, env.deploy_project_root))


def install_mkvirtualenv():
    print('installing mkvirtualenv...')
    sudo('apt-get install python-pip python-dev build-essential -y')
    sudo('pip install virtualenv virtualenvwrapper')
    sudo('echo "export WORKON_HOME=~/Code/.ve" >> .bash_profile')
    sudo('mkdir -p ~/Code/.ve')
    sudo('chmod 755 -R ~/Code/.ve')
    sudo('echo "source /usr/local/bin/virtualenvwrapper.sh" >> .bash_profile')
    sudo('source .bash_profile')
    # sudo('sudo pip3 install --upgrade pip')


def install_redis():
    # installs redis-server
    print('installing redis-server...')
    sudo('apt-get update')
    sudo('apt-get upgrade')
    sudo('apt-get install redis-server -y')


def create_ve():
    # creates virtualenv
    print('creating virtualenv...')
    sudo('mkvirtualenv v-kw-scraper --python=/usr/bin/python3')


def install_requirements():
    # installs projects requirements using pip
    print('installing requirements...')
    env.project_requirements = env.deploy_project_root + env.deploy_current_dir + '/requirements.txt'
    sudo('workon v-kw-scraper;pip install -r {}'.format(env.project_requirements))


def cleanup():
    # remove any artifacts of the deploy process
    print('cleanup...')
    local('rm -rf {}.zip'.format(env.archive_name))

    print('Rebooting please wait till server is back on')
    with warn_only():
        reboot()


############################################################
# Action Functions
############################################################
def deploy():
    """This function will be used for upgrading the code"""
    # specify path to deploy root dir - you need to create this
    env.deploy_project_root = '/home/{}/Code/'.format(env.user)

    upload_archive()
    copy_code()
    configure_services()
    install_requirements()
    # create_version()
    cleanup()
    print('deploy complete!')


def init():
    # specify path to deploy root dir - you need to create this
    env.deploy_project_root = '/home/{}/Code/'.format(env.user)

    """This function will be used for setting the basic robot"""
    install_essentials()
    install_mkvirtualenv()
    install_redis()
    create_log_dir()
    create_ve()
    print('deploy complete!')
