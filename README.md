# kw scrapper
The service will scrape

## about the request
example for a curl call is:
```
curl http://talbenbasat.hiring.keywee.io/stories?url=facebook.com --header "Content-Type: application/json" \
  --request POST
```

the testing was done with postman.
you can also add reset: true to the body of the request to get a new response.

the second request will be served from redis cache.
```
{"description":"The Open Graph protocol enables any web page to become a rich object in a social graph.",
"id":3,
"image":{"alt":"The Open Graph logo","height":"300","type":"image/png","url":"http://ogp.me/logo.png","width":"300"},
"scrape_status":"done",
"title":"Open Graph protocol",
"type":"website",
"updated_time":"2018-08-19 06:46:06.621640",
"url":"http://ogp.me/"}

```

in the case that image or other tag contains more than one value, it will transform into an array.
example can be displayed in facebook.com page.

## Installation
* install python
* install virtualenvwrapper
```mkvirtualenv --python=`which python3` v-kw-scrapper```
* install redis
* install nginx
* copy code and create needed folders
* run the scraper service
* redirect the nginx code

for the deployment there is a fabric code
`fab server1 init` - for init
`fab server1 deploy` - for deploy


## issues
* in the case the the server will be shutdown the ids will reset.
in order to fix this we can use sql server like sqllite
* if we desire that image and audio will always be an array we can do
a specifc convert for them in the code

## Additional information
https://github.com/erikriver/opengraph
